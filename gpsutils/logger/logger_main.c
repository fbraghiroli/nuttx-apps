#include <nuttx/config.h>
#include <stdio.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <nuttx/fs/ioctl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <nuttx/input/buttons.h>
#include <arch/board/board.h>

#define GPS_UART        "/dev/ttyS1"
#define GPS_UART_SPEED  9600

/* This is hardcoded here: configs/stm32f103-minimum/src/stm32_bringup.c */
#define BTNS_DEV        "/dev/buttons"

#define LOG_DIRPATH     "/mnt"

static struct priv {
	int fd;
	uint32_t tot_w;
	int32_t bs;
	FILE *log_f;
	int btns_fd;
	btn_buttonset_t btn_data;
	uint8_t file_log;
	uint8_t stdout_log;
} prv;

static int uart_init(void)
{
	struct termios tio_uart;

	prv.fd = open(GPS_UART, O_RDONLY);
	if (prv.fd < 0)
		return -errno;

	if (tcgetattr(prv.fd, &tio_uart) < 0)
		return -errno;
	cfsetispeed(&tio_uart, GPS_UART_SPEED);
	cfsetospeed(&tio_uart, GPS_UART_SPEED);
	tio_uart.c_cflag |= CLOCAL; /* Do not change "owner" of port */
	tio_uart.c_cflag |= CREAD; /* Enable receiver*/
	/* 8N1 */
	tio_uart.c_cflag &= ~PARENB;
	tio_uart.c_cflag &= ~CSTOPB;
	tio_uart.c_cflag &= ~CSIZE; /* Mask the character size bits */
	tio_uart.c_cflag |= CS8;
	/* Hardware flow control disabled */
	tio_uart.c_cflag &= ~(CCTS_OFLOW | CRTS_IFLOW);
	/* Raw Input */
	tio_uart.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
	/* Apply the options */
	if (tcsetattr(prv.fd, TCSANOW, &tio_uart) < 0)
		return -errno;
	return 0;
}

FILE * try_file_log(char *filepath)
{
	FILE *fp;
	struct stat sb;
	int fn = 0;

	if (stat(LOG_DIRPATH, &sb) != 0 || !S_ISDIR(sb.st_mode)) {
		printf("%s not mounted\n", LOG_DIRPATH);
		return NULL;
	}

	for (fn = 0; fn < 255; fn++) {
		sprintf(filepath, "%s/%d.txt", LOG_DIRPATH, fn);
		printf("Trying %s... ", filepath);
		if (stat(filepath, &sb) == 0 && S_ISREG(sb.st_mode)) {
			printf("exists\n");
		} else {
			printf("\nUsing %s\n", filepath);
			break;
		}
	}

	fp = fopen(filepath, "w+");

	if (!fp) {
		printf("Failed to open %s (-%d)", filepath, errno);
		return NULL;
	}

	return fp;
}

unsigned char read_buf[256 + 1];

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int logger_main(int argc, char *argv[])
#endif
{
	int ret;
	char filepath[10];

	printf("logger main!\n");
	prv.stdout_log = 1;
	prv.file_log = 1;
	prv.bs = 4*1024;

	if ((ret = uart_init()) < 0) {
		printf("Gps uart init fail! (%d)", ret);
		ret = 1;
		goto exit_ret;
	}

	usleep(500000); /* Be sure to have stable voltage on button pin */
	prv.btns_fd = open(BTNS_DEV, O_RDONLY | O_NONBLOCK);
	if (prv.btns_fd != -1) {
		if (read(prv.btns_fd, &prv.btn_data, sizeof(btn_buttonset_t)) > 0) {
			if (!(prv.btn_data & BUTTON_USER1_BIT))
				prv.stdout_log = 0;
		}
		close(prv.btns_fd);
	}

	if (!(prv.log_f = try_file_log(filepath)))
		prv.file_log = 0;

	if (!prv.file_log && !prv.stdout_log)
		goto exit_close_f;

	while(1) {
		int n_read;
		n_read = read(prv.fd, read_buf, 256);
		if (n_read < 1) {
			printf("read error, wait 2s\n");
			sleep(2);
			continue;
		}

		if (prv.file_log) {
			if ((ret = fwrite(read_buf, 1, n_read, prv.log_f)) != n_read) {
				if (!prv.stdout_log)
					printf("Written %d, expected %d", ret, n_read);
			}

			prv.tot_w += n_read;
			prv.bs -= n_read;

			if (prv.bs <= 0) {
				//fflush(prv.log_f);
				fclose(prv.log_f);
				prv.log_f = fopen(filepath, "a+");
				prv.bs = 4*1024;
			}
		}

		if (prv.stdout_log) {
			read_buf[n_read] = '\0';
			printf("%s", read_buf);
		}
	}

exit_close_f:
	fclose(prv.log_f);
exit_close_gps:
	close(prv.fd);
exit_ret:
	printf("Exit\n");
	return ret;
}
